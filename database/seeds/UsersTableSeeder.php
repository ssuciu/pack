<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Suciu Sergiu Ionut",
            'email' => 'suciu.sergiu94@gmail.com',
            'username' => 'admin',
            'password' => Hash::make('test')
        ]);
        DB::table('users')->insert([
            'name' => "Suciu Sergiu Ionut",
            'email' => 'suciu.sergiu94@gmail.com',
            'username' => 'hr',
            'password' => Hash::make('test')
        ]);
    }
}
