jQuery(document).ready(function () {
    var $searchField = $("#search-by-name");
    AppTable.init('employees_table', {
        formId: 'form_station_search',
        layout: {class:'customized-data-table'},
        autoWidth: true,
        data: {
            source: {
                read: {
                    url: '/employees/list',
                    params: {
                        _token :  $('meta[name="token"]').attr('content')
                    }
                }
            }
        },
        columns: [{
            field: "created_at",
            title: i18next.t('added_at'),
            sortable: false, // disable sort for this column
            width: 110,
            textAlign: 'center',
            template: function (row) {
                return moment(row.created_at.split(' ')[0]).format('DD-MM-YYYY');

            }
        },{
            field: "actions",
            width: 150,
            title: i18next.t('actions'),
            orderable:false,
            textAlign: 'center',
            sortable: false,
            template: function (row) {
                return '\
						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill view-employee-documents" title="' + i18next.t('view_doc') + '">\
							<i class="la la-files-o"></i>\
						</a>\
						<a href="/employees/' + row.number + '" target="_blank" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill print-employee-documents" title="' + i18next.t('down_details') + '">\
						    <i class="la la-download"></i>\
						</a>\
						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill edit-employee" title="' + i18next.t('edit_details') + '">\
							<i class="la la-edit"></i>\
						</a>\
                        <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill remove-employee" title="' + i18next.t('delete') + '">\
                            <i class="la la-trash"></i>\
                        </a>';
            }
        }, {
            field: "name",
            title: i18next.t('name'),
            width: 150,
            template: function (row) {
                // callback function support for column rendering
                return row.first_name + ' ' + row.last_name;
            }
        }, {
            field: "birthday",
            title: i18next.t('bdate'),
            sortable: false, // disable sort for this column
            width: 100
        }, {
            field: "address",
            title: i18next.t('address')
        }, {
            field: "email",
            title: i18next.t('email'),
            template: function (row) {
                return '<span title="' + row.email + '">' + row.email + '</span>'
            }
        }, {
            field: "phone",
            title: i18next.t('phone_nr'),
            sortable: false
        }, {
            field: "ssn",
            title: i18next.t('ssn')
        }],
        columnDefs: [
            { targets: [0, 1, 2, 3 ,4 ,5,  6, 7,8], orderable: false},
            { targets: '_all', visible: false }
        ]
    });

    var deleteEmployee = function (employeeId) {
        $.ajax({
            type: "POST",
            url: "/employees/"+employeeId,
            data: {
                _method: "delete",
                _token: $('meta[name="token"]').attr('content')
            },
            dataType: 'json',
            success: function () {
                ModalHelperConfirm.close();
                AppTable.refreshTable();
                Notify.success(i18next.t('success'), i18next.t('emp_deleted'));
            },
            error: function () {
                Notify.error(i18next.t('error'), i18next.t('error_msg'));
            }
        });
    };

    var $body = $('body');
    $body.on('click', '.remove-employee', function () {
        var employeeId = $(this).closest('tr').data('id');
        var name = $(this).closest('tr').find('td[data-field="name"]').find('span').html();
        var options = {
            title : i18next.t('delete_emp'),
            content: i18next.t('delete_emp_p') + name + '?',
            confirmButtonLabel: i18next.t('delete')
        };

        ModalHelperConfirm.confirm(options, function () {
            return deleteEmployee(employeeId);
        }, employeeId);
    });

    $body.on('click', '.view-employee-documents', function () {
        var employeeId = $(this).closest('tr').data('id');
        var name = $(this).closest('tr').find('td[data-field="name"]').find('span').html();
        var options = {
            title : i18next.t('view_for') + name,
            url : 'employees/' + employeeId + '/photos',
            id: employeeId
        };
        SlideshowModal.init(options);
    });

    $body.on('click', '.edit-employee', function () {
        var employeeId = $(this).closest('tr').data('id');
        window.location.href = "/employees/" + employeeId + "/edit"
    });

    $searchField.on('change keyup paste',function () {
        var val = $(this).val();
        if(_.isString(val)) {
            val = val.trim();
        }
        if(val) {
            AppTable.refreshTable($(this));
        }
    });

});
