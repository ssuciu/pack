$(document).ready(function () {
   var siema = new Siema({
       'loop' : true
   });
    document.querySelector('.siema-prev').addEventListener('click', function () {
        siema.prev()
    });
    document.querySelector('.siema-next').addEventListener('click', function () {
        siema.next()
    });


    function deleteDocument(photoId, slideIndex, count) {
        $.ajax({
            url : '/employees/documents/' + photoId,
            type: 'post',
            data : {
                _method : 'delete',
                _token : $('meta[name="token"]').attr('content'),
                id : photoId
            },
            success : function () {
                siema.goTo(0);
                siema.remove(slideIndex);
                Notify.success(i18next.t('success'), i18next.t('deleted_doc'));
                ModalHelperConfirm.close();
                if(count === 1) {
                    $("#regular-content").remove();
                    $("#no-documents").attr('hidden', false);
                }
            },
            error: function () {
                Notify.error(i18next.t('error'), i18next.t('error_msg'))
            }

        })
    }

    $("#delete-photo-button").click(function () {

        var img = $(siema.selector).find('img:eq(' + siema.currentSlide + ')'),
            imgCount = $(siema.selector).find('img').length,
            photoId = $(img).data('id');
        console.log(photoId);

        var options = {
            title : i18next.t('delete_doc'),
            content: i18next.t('delete_slide_p'),
            confirmButtonLabel: i18next.t('delete')
        };

        ModalHelperConfirm.confirm(options, function () {
            return deleteDocument(photoId, siema.currentSlide, imgCount);
        }, photoId);
    })
});