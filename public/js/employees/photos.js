$(document).ready(function () {
    Dropzone.options.documentsdropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 10,
        maxFilesize: 5, // MB
        acceptedFiles: "image/*",
        headers: {
            'X-CSRFToken': $('meta[name="token"]').attr('content')
        }
    };



    $("#done-adding-photos").click(function () {
        window.location.href = "/employees"
    });
});