$(document).ready(function () {
    var $form = $("#add-employees-form");

    $('#close-button').click(function () {
        window.location.href = "/employees"
    });

    $(".datepicker").datepicker({
        'orientation':"bottom left",
        'templates': {
            'leftArrow':'<i class="la la-angle-left"></i>',
            'rightArrow':'<i class="la la-angle-right"></i>'
        },
        'startView': "decades",
        'weekStart' : 1,
        'format' : 'dd-mm-yyyy',
        'autoclose': true
    });

    $("#create-employee-button").click(function () {
        $form.validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                phone: {
                    required:true,
                    minlength: 10
                },
                email: {
                    email: true
                },
                address: {
                    required: true
                },
                ssn: {
                    required: true
                },
                birthday: {
                    required: true
                }
            }
        });
        if (!$form.valid()) {
            return;
        }
    });
});
