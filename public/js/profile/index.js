$(document).ready(function () {
   $('#locale-dropdown').change(function () {
       var locale = $(this).val();
       window.location.href = '/language/' + locale;
   });
});