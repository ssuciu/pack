var Notify = function () {

    return {
        defaultOptions : {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        },

        success : function (title, message, callback, options) {
            this._extendOptions(callback, options);
            toastr.success(message, title);
            toastr.options = this.defaultOptions;
        },

        info : function (title, message, callback, options) {
            this._extendOptions(callback, options);
            toastr.info(message, title);
            toastr.options = this.defaultOptions;
        },

        warning : function (title, message, callback, options) {
            this._extendOptions(callback, options);
            toastr.warning(message, title);
            toastr.options = this.defaultOptions;
        },

        error : function (title, message, callback, options) {
            this._extendOptions(callback, options);
            toastr.error(message, title);
            toastr.options = this.defaultOptions;
        },

        _extendOptions : function (callback, options) {
            var newOptions = this.defaultOptions;
            if (options) {
                $.extend(newOptions, options);
            }
            if (callback) {
                newOptions.onclick = callback;
            }
            toastr.options = newOptions;
        }
    }
}();