var SlideshowModal = function() {
    return {
        lastURL : "",
        options: {
            title: 'slideshow',
            url: '/employees/photos',
            id: ''
        },
        init: function(options) {
            $("#myCarousel").carousel();
            this.options = options;
            this.show();
        },
        buildCarouserl: function (url, callback, id) {
            $.ajax({
                type: 'get',
                url : url,
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.length) {
                        var $toAppendDiv = $("#slideshow-append-div"),
                            divs = "";
                        for (var i = 0; i < data.length ; i++) {
                            var name = data[i].file_name;
                            var div = '<div style="margin-top: 10px;margin-bottom: 10px" class="item active"><img style="width: 100%" class="img-responsive" src="/storage/documents/'+ id + "/" + name  + '"></div>';
                            divs+= div;
                        }
                        $toAppendDiv.html(divs);

                        callback(url);
                    } else {
                        this.lastURL = "";
                        Notify.warning('', i18next.t('no_pic'));
                    }
                }
            })
        },
        show: function() {
            if (this.options.title) {
                $('.slideshow-modal-title').html(this.options.title);
            }
            var requestUrl = this.options.url;
            if(this.lastURL !== requestUrl) {
                this.buildCarouserl(this.options.url, this.open, this.options.id)
            } else {
                this.open();
            }
        },

        close: function() {
            $("#slideshow").modal('hide');
        },

        open: function (url) {
            if(url) {
                this.lastURL = url;
            }

            $("#slideshow").modal();
        }
    };
}();
