i18next.init({
    lng: document.documentElement.lang,
    fallbackLng: 'en',
    resources: Locale.translations()
});