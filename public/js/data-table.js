var AppTable = function () {
    var prepareData = function(params) {
        var settings = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '',
                        params: {
                            _token :  $('meta[name="token"]').attr('content')
                        }
                    }
                },
                pageSize: 5,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },
            sortable: true,
            filterable: true,
            pagination: true,
            processing: true,
            serverSide: true,
            toolbar: {
                layout: ['pagination', 'info'],
                placement: ['bottom'],
                items: {
                    pagination: {
                        type: 'default',
                        pages: {
                            desktop: {
                                layout: 'default',
                                pagesNumber: 6
                            },
                            tablet: {
                                layout: 'default',
                                pagesNumber: 3
                            },
                            mobile: {
                                layout: 'compact'
                            }
                        },

                        // navigation buttons
                        navigation: {
                            prev: true, // display prev link
                            next: true, // display next link
                            first: true, // display first link
                            last: true // display last link
                        },

                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50, 100/*, -1*/] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },

                    // records info
                    info: true
                }
            },
            translate: {
                records: {
                    processing: i18next.t('waiting'),
                    noRecords: i18next.t('no_records')
                },
                toolbar: {
                    pagination: {
                        items: {
                            default: {
                                first: i18next.t('first'),
                                prev: i18next.t('prev'),
                                next: i18next.t('next'),
                                last: i18next.t('last'),
                                more: i18next.t('more_p'),
                                input: i18next.t('page_nr'),
                                select: i18next.t('select_nr')
                            },
                            info: i18next.t('d_info', {
                                interpolation: {
                                    sufix : '>>',
                                    prefix: '<<'
                                }
                            })
                        }
                    }
                }
            }
        };

        settings.data.source.read.url = params.data.source.read.url
        settings.data.source.read.params = params.data.source.read.params;
        delete params.data;

        params.columns.push({
            field:"identifier",
            title:"identifier",
            responsive: {hidden: 'lg'},
            template: function (row) {
                $("[data-row='" + row.rowIndex + "']").attr('data-id', row.number);
            }
        });

        return $.extend(settings, params);

    };

    return {
        dataTable: null,
        formId: null,

        init: function (tableName, params) {
            //get the filter form id
            if (params.formId != null) {
                AppTable.formId = params.formId;
                delete params.formId;
            }

            var settings = prepareData(params);
            AppTable.dataTable = $('#' + tableName).mDatatable(settings);
            return AppTable.dataTable;
        },

        refreshTable: function (searchField) {
            var thisDataTable = AppTable.dataTable;
            if(searchField && searchField.val()) {
                var name = searchField.attr('name');
                var val = searchField.val();
                thisDataTable.setDataSourceQuery(
                    {
                        name : val
                    }
                );
            }
            thisDataTable.reload();
        }
    }
}();

