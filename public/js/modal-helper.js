var ModalHelperConfirm = function() {
    return {
        callback: null,
        params: null,

        init: function () {
            $('body').on("click", ".modal-confirm", function() {
                if (typeof ModalHelperConfirm.callback == 'function') {
                    ModalHelperConfirm.callback(ModalHelperConfirm.params);
                }
            });
        },
        confirm: function(options, callback, params) {
            this.callback = callback;
            this.params = params;

            if (options.title) {
                $('.confirm-modal-title').html(options.title);
            }

            if (options.content) {
                $('.confirm-content').html(options.content);
            }

            if (options.confirmButtonLabel) {
                $('.modal-confirm').html(options.confirmButtonLabel);
            }

            this.open();
        },

        close: function() {
            this.hideError();
            $("#confirm").modal('hide');
        },

        open: function () {
            this.hideError();
            $("#confirm").modal();
        },

        hideError: function() {
            $(".alert-danger").hide();
        }
    };
}();

jQuery(document).ready(function () {
    ModalHelperConfirm.init();
});