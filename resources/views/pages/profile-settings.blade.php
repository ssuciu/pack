@extends('layouts.app')
@section('title')
    @lang('common.a_p_settings')
@endsection
@section('portlet-header')
    @lang('common.preferences')
@endsection
@section('header')
    @include('includes.breadcumb', [
            'module' => Lang::get('common.a_p_settings'),
            'menues' => [[
                'route' => '#',
                'name' => Lang::get('common.a_p_settings')
                ]
            ]])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="m-card-user">
                <div class="m-card-user__pic">
                    @if($user->photo)
                        <img src="{{asset('media/' . $user->photo)}}" class="m--img-rounded m--marginless" alt="">
                    @else
                        <img src="{{asset('media/icon-user-default.png')}}" class="m--img-rounded m--marginless"
                             alt="">
                    @endif
                </div>
                <div class="m-card-user__details">
                    @if($user->name)
                        <span class="m-card-user__name m--font-weight-500 profile-name">{{$user->name}}</span>
                    @endif
                    @if($user->email)
                        <a href="#" class="m-card-user__email m--font-weight-300 m-link">{{$user->email}}</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="form-group m-form__group row">
                <label for="locale-dropdown" class="col-form-label col-lg-3 col-md-3 col-sm-3">
                    @lang('common.language'):
                </label>
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <div class="btn-group bootstrap-select form-control m-bootstrap-select m_">
                        <select id="locale-dropdown" class="selectpicker">
                        @foreach(config('languages') as $locale => $name)
                                <option @if(session()->get('locale') === $locale) selected @endif value="{{$locale}}" data-content="<span>{{$name}}</span><img style='padding-right:25px;padding-top:3px' class='pull-right' src='{{asset('media/' . $locale . '.png')}}' />"></option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-footer-scripts')
    <script src="{{asset('js/bootstrap-select.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/profile/index.js')}}" type="text/javascript"></script>
@endsection