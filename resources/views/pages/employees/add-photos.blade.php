@extends('layouts.app')

@section('title')
    @lang('employees.documents')
@endsection


@section('portlet-healer-icon')
    <span class="m-portlet__head-icon"><i class="fa fa-film"></i></span>
@endsection


@section('portlet-header')
    @lang('employees.add_documents_for') {{$employee->first_name . ' ' . $employee->last_name}}
@endsection


@section('header')
    @include('includes.breadcumb', [
            'module' => Lang::get('employees.employees_lbl'),
            'menues' => [[
                'route' => '/employees',
                'name' => Lang::get('employees.menu_1')
                ],[
                'route' => '#',
                'name' => Lang::get('employees.menu_2')
                ]
            ]])
@endsection


@section('content')
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-3 col-sm-12">
            @lang('employees.add_documents_phots')
        </label>
        <div class="col-lg-4 col-md-9 col-sm-12">
            <form class="m-dropzone dropzone m-dropzone--brand dz-clickable"
                 action="{{route('employees.upload-documents', ['employee' => $employee->id])}}" id="documentsdropzone">
                {{csrf_field()}}
                <div class="m-dropzone__msg dz-message needsclick">
                    <h3 class="m-dropzone__msg-title">
                        @lang('employees.add_documents_phots')
                    </h3>
                    <span class="m-dropzone__msg-desc">@lang('employees.only_images')</span>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 ml-lg-auto">
            <button type="button" class="btn btn-brand" id="done-adding-photos">
                @lang('common.done')
            </button>
        </div>
    </div>
@endsection


@section('extra-footer-scripts')
    <script src="{{asset('js/employees/photos.js')}}"></script>
@endsection