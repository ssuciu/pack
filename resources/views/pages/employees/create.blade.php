@extends('layouts.app')
@section('title')
    @lang('employees.add_page')
@endsection
@section('portlet-header')
    @lang('employees.new_page')
@endsection
@section('header')
    @include('includes.breadcumb', [
            'module' => Lang::get('employees.employees_lbl'),
            'menues' => [[
                'route' => '/employees',
                'name' => Lang::get('employees.menu_1')
                ],[
                'route' => '#',
                'name' => Lang::get('employees.menu_add')
                ]
            ]])
@endsection
@section('content')
    <form id="add-employees-form" method="post" action="{{route('employees.store')}}">
        {{csrf_field()}}
        <div class="m-form__section m-form__section--first">
            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">
                    @lang('employees.f_name'):
                </label>
                <div class="col-lg-6">
                    <input type="text" name="first_name" class="form-control m-input" placeholder="@lang('employees.f_name_prompt')">
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">
                    @lang('employees.l_name'):
                </label>
                <div class="col-lg-6">
                    <input type="text" name="last_name" class="form-control m-input" placeholder="@lang('employees.l_name_prompt')">
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">
                    @lang('employees.contact')
                </label>
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="email" name="email" class="form-control m-input has-addon" placeholder="@lang('employees.email_prompt')">
                        <span class="input-group-addon"><i class="la la-envelope-o"></i></span>
                    </div>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label"></label>
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" name="phone" class="form-control m-input has-addon" placeholder="@lang('employees.phone_prompt')">
                        <span class="input-group-addon"><i class="la la-phone"></i></span>
                    </div>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">
                    @lang('employees.birthdate'):
                </label>
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" name="birthday" class="form-control m-input datepicker has-addon" placeholder="@lang('employees.birthdate')">
                        <span class="input-group-addon"><i class="la la-calendar-o"></i></span>
                    </div>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">
                    @lang('employees.address'):
                </label>
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" name="address" class="form-control m-input has-addon" placeholder="@lang('employees.address_prompt')">
                        <span class="input-group-addon"><i class="la la-map-marker"></i></span>
                    </div>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">
                    @lang('employees.health_care'):
                </label>
                <div class="col-lg-6">
                    <input type="text" name="ssn" class="form-control m-input" placeholder="@lang('employees.ssn')">
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-lg-9 ml-lg-auto">
                    <button type="submit" class="btn btn-brand" id="create-employee-button">
                        @lang('common.submit')
                    </button>
                    <button type="reset" class="btn btn-secondary" id="close-button">
                        @lang('common.close')
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('extra-footer-scripts')
    <script src="{{asset('js/employees/create.js')}}"></script>
@endsection