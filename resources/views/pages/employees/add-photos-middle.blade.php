@extends('layouts.app')
@section('content')
    <input type="hidden" @if($employee) value="{{$employee->id}}" @endif id="employee-id">
@endsection
@section('extra-footer-scripts')
    <script>
        $(document).ready(function () {
            var id = $("#employee-id").val();
            if(id) {
                window.location.href = "/employees/" + id + "/documents"
            } else {
                window.location.href = "/employees/"
            }
        })
    </script>
@endsection