<html>
<head>
    <script type="text/css">
        html {font-family: Poppins, sans-serif}
    </script>
</head>
<body>
<h1 align="center">@lang('employees.data')</h1>
<br />
<br />
<div class="container">
    <div class="row">
        <div style="width: 33%;display: inline-block">
            @lang('employees.f_name'):
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->first_name}}
        </div>
        <div style="width: 33%;display: inline-block">
            @lang('employees.l_name'):
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->last_name}}
        </div>
        <div style="width: 33%;display: inline-block">
            @lang('employees.birthdate'):
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->birthday}}
        </div>
        <div style="width: 33%;display: inline-block">
            @lang('employees.address'):
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->address}}
        </div>
        <div style="width: 33%;display: inline-block">
            @lang('employees.ssn'):
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->ssn}}
        </div>
        <div style="width: 33%;display: inline-block">
            @lang('employees.phone'):
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->phone}}
        </div>
        <div style="width: 33%;display: inline-block">
            @lang('employees.email')
        </div>
        <div style="width: 66%;display: inline-block">
            {{$employee->email}}
        </div>
    </div>
    <br/>
    <div class="row">
        @foreach($employee->getPhotos() as $photo)
            <div><img style="width: 100%" class="img-responsive" src="/storage/documents/{{$employee->id}}/{{$photo->file_name}}"></div>
        @endforeach
    </div>
</div>



<script src="{{asset('js/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('js/scripts.bundle.js')}}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        window.print();
    })
</script>
</body>
</html>