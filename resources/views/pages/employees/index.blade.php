@extends('layouts.app')
@section('title')
    @lang('employees.employees_vis')
@endsection
@section('portlet-header')
    @lang('employees.employees_lbl')
@endsection
@section('portlet-header-button')
    <a href="{{route('employees.create')}}" class="btn btn-brand">+ @lang('employees.add')</a>
@endsection
@section('header')
    @include('includes.breadcumb', [
            'module' => Lang::get('employees.employees_lbl'),
            'menues' => [[
                'route' => '#',
                'name' => Lang::get('employees.menu_1')
                ]
            ]])
@endsection
@section('content')
    <div>
                <div class="float-left">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-search"></i></span>
                        <input type="text" name="name" id="search-by-name" class="form-control" placeholder="@lang('employees.search_name')" aria-describedby="sizing-addon2">
                    </div>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <div>
        <div class="m_datatable" id="employees_table"></div>
    </div>
    @include('includes.delete-modal')
    @include('includes.images-modal')
@endsection
@section('extra-footer-scripts')
    <script src="{{asset('js/data-table.js')}}"></script>
    <script src="{{asset('js/modal-helper.js')}}"></script>
    <script src="{{asset('js/images-modal.js')}}"></script>
    <script src="{{asset('js/employees/index.js')}}"></script>
@endsection