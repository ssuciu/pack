@extends('layouts.app')
@section('title')
    @lang('employees.remove_doc')
@endsection
@section('portlet-header')
    @lang('employees.remove_doc_for') {{$employee->first_name . " " . $employee->last_name}}
@endsection


@section('header')
    @include('includes.breadcumb', [
            'module' => Lang::get('employees.employees_lbl'),
            'menues' => [[
                'route' => '/employees',
                'name' => Lang::get('employees.menu_1')
                ],[
                'route' => '/employees/' . $employee->id . '/edit',
                'name' => Lang::get('employees.edit'). " " . $employee->first_name . " " . $employee->last_name
                ],[
                'route' => '#',
                'name'  => 'Delete documents'
                ]
            ]])
@endsection
@section('content')
    @if($photos)
        <div id="regular-content">
            <div class="center-block" id="to-center-in">
                <div class="btn-group m-btn-group m-btn-group--pill" id="centered-in" role="group" aria-label="...">
                    <button type="button" class="m-btn btn btn-brand siema-prev">
                        <<
                    </button>
                    <button id="delete-photo-button" type="button" class="m-btn btn btn-brand">
                        <i class="la la-trash-o"></i>
                    </button>
                    <button type="button" class="m-btn btn btn-brand siema-next">
                        >>
                    </button>
                </div>
            </div>
            <br/>
            <div class="siema">
                @foreach($photos as $photo)
                    <img data-id="{{$photo->id}}" style="width: 100%; height: 100%" src="{{asset('storage/documents') . "/" . $employee->id . "/" . $photo->file_name}}">
                @endforeach
            </div>
        </div>
        <div hidden id="no-documents">
            <h1>{{Lang::get('employees.no_documents_for', ['name' => $employee->first_name . " " . $employee->last_name])}}<a href="/employees">@lang('employees.employees_lbl')</a></h1>
        </div>
    @else
        <script>
            window.location.href = '/employees'
        </script>
    @endif


    @include('includes.delete-modal')
@endsection
@section('extra-footer-scripts')
    <script src="{{asset('js/modal-helper.js')}}"></script>
    <script src="{{asset('js/vendor/siema.js')}}"></script>
    <script src="{{asset('js/employees/delete-photos.js')}}"></script>
@endsection