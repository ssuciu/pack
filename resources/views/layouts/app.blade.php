<!DOCTYPE html><!--
-->
<html lang="{{ app()->getLocale() }}" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Foldy Pac | @yield('title')
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{asset('css/vendor.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{asset('media/favicon.ico')}}" />
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header class="m-grid__item    m-header " data-mini data-minimize-mobile-offset="200"  id="theme-header">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">

                        <div class="m-stack__item m-stack__item--middle m-brand__tools">
                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:;"
                               id="m_aside_left_minimize_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" id="theme-body">
        <!-- BEGIN: Left Aside -->
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu"
                    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                    data-menu-vertical="true"
                    data-menu-scrollable="false" data-menu-dropdown-timeout="500">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <li class="m-menu__section" hidden>
                        <h4 class="m-menu__section-text">@lang('common.welcome_c'),  {{Auth::user()->username}}</h4>
                    </li>
                    <li class="m-menu__item @if(isset($on_dashboard)) m-menu__item--active @endif" hidden aria-haspopup="true" >
                        <a  href="{{route('home')}}" class="m-menu__link">
                            <i class="m-menu__link-icon flaticon-line-graph"></i>
                            <span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Dashboard
											</span>
										</span>
									</span>
                        </a>
                    </li>
                    <li class="m-menu__section">
                        <h4 class="m-menu__section-text">
                            @lang('common.module')
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li class="m-menu__item @if(isset($on_employees)) m-menu__item--active @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                        <a  href="{{route('employees.index')}}" class="m-menu__link">
                            <i class="m-menu__link-icon flaticon-layers"></i>
                            <span class="m-menu__link-text">@lang('common.employees')</span>
                        </a>

                    <li class="m-menu__section">
                        <h4 class="m-menu__section-text">@lang('common.ac_actions')</h4>
                    </li>
                    <li class="m-menu__item @if(isset($on_account_settings)) m-menu__item--active @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
                        <a  href="{{route('profile')}}" class="m-menu__link">
                            <i class="m-menu__link-icon flaticon-clipboard"></i>
                            <span class="m-menu__link-text">@lang('common.ac_settings')</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true"  data-menu-submenu-toggle="hover">
                        <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="m-menu__link">
                            <i class="m-menu__link-icon flaticon-logout"></i>
                            <span class="m-menu__link-text">@lang('common.logout')</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                    @yield('header')
                    </div>
                    <div>
                        <img src="{{asset('media/logo.png')}}">
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                @yield('portlet-healer-icon')
                                <h3 class="m-portlet__head-text">
                                    @yield('portlet-header')
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            @yield('portlet-header-button')
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
                                2017 &copy; Web application created with <span style="color: darkred;font-size: 17px">&hearts;</span> by
								<a href="#" class="m-link" target="_blank">
									Suciu Sergiu
								</a>
							</span>
                </div>
            </div>
        </div>
    </footer>
    <form method="post" action="{{route('logout')}}" style="display: none" id="logout-form">
        {{ csrf_field() }}
    </form>
    <!-- end::Footer -->
</div>
<script src="{{asset('js/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('lang/locale.js')}}" type="text/javascript"></script>
<script src="{{asset('js/vendor/i18next.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('js/vendor/lodash.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-extends.js')}}" type="text/javascript"></script>
<script src="{{asset('js/notify.js')}}" type="text/javascript"></script>
@yield('extra-footer-scripts')
</body>
</html>


