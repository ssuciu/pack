<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="confirm_modal_label" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="confirm-modal-title" id="confirm_modal_label">
                    Modal title
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-alert m-alert--icon alert alert-danger m-alert--air m-alert--outline m--hide" role="alert" id="confirm_modal_error_message">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text content-error">@lang('common.admin_error')</div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close"></button>
                    </div>
                </div>
                <p class="confirm-content">
                    @lang('common.remove_request')
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary btn-confirm-cancel" data-dismiss="modal">
                    @lang('common.close')
                </button>
                <button type="button" class="btn btn-sm btn-primary btn-confirm modal-confirm">
                    @lang('common.confirm')
                </button>
            </div>
        </div>
    </div>
</div>