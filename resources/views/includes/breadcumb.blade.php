<h3 class="m-subheader__title m-subheader__title--separator">{{$module}}</h3>
    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
            <a href="/home" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i>
            </a>
        </li>
        @if(count($menues))
            <li class="m-nav__separator">
                -
            </li>
        @endif
        @foreach($menues as $menu)
            <li class="m-nav__item">
                <a href="{{$menu['route']}}" class="m-nav__link">
                <span class="m-nav__link-text">
                    {{$menu['name']}}
                </span>
                </a>
            </li>
            @if (!$loop->last)
                <li class="m-nav__separator">
                    -
                </li>
            @endif
        @endforeach
    </ul>