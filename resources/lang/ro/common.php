<?php
return [
    'welcome_c' => 'Salut',
    'module' => 'Module',
    'employees' => 'Angajati',
    'ac_settings' => 'Setari cont',
    'ac_actions' => 'Actiuni legate de cont',
    'logout' => 'Logout',
    'admin_error' => 'Eroare. Va rugam sa contactati administratorul tehnic.',
    'remove_request' => 'Sunteti sigur ca doriti sa stergeti aceasta inregistrare?',
    'close' => 'Inchide',
    'confirm'=> 'Confirma',
    'language' => 'Limba',
    'a_p_settings' => 'Setari Cont',
    'preferences' => 'Preferinte',
    'done' => 'Am terminat',
    'submit' => 'Trimite',
    'update' => 'Modifica'

];