<?php
return [
    'documents' => 'Documentele Angajatilor',
    'employees_lbl' => 'Angajati',
    'add_page' => 'Pagina adaugare angajati',
    'data' => 'Date angajati',
    'new_page' => 'Angajat nou',
    'add' => 'Adauga angajat',
    'edit' => 'Modifica angajat',
    'add_documents_for' => 'Adauga documente angajat',
    'add_doc' => 'Adauga documente',
    'remove_doc' => 'Sterge documente',
    'add_documents_phots' => 'Adauga imagini cu documentele angajatului',
    'drop_click' => 'Trage imaginile cu documente aici sau da click pentru a le incarca.',
    'only_images' => 'Doar fisierele de tip imagine o sa fie incarcate',
    'f_name' => 'Nume',
    'l_name' => 'Prenume',
    'contact' => 'Contact',
    'birthdate' => 'Data nasterii',
    'address' => 'Adresa',
    'health_care' => 'Asigurare sanatate',
    'address_prompt' => 'Adresa curenta',
    'f_name_prompt' => 'Numele angajatului',
    'l_name_prompt'=> 'Prenumele angajatuli',
    'email_prompt' => 'Emailul',
    'phone_prompt' => 'Numarul de telefon',
    'ssn' => 'Numar asigurare',
    'phone' => 'Telefon',
    'email' => 'Adresa de email',
    'employees_vis' => 'Vizualizare angajati',
    'search_name' => 'Cauta dupa nume...',
    'remove_doc_for' => 'Sterge documentele lui',
    'no_documents_for' => ':name nu mai are nici un docment asociat, mergi la ',
    'edit_page' => 'Pagina modificare angajat',
    'edit_for' => 'Mofifica datele pentru :name',
    'menu_1' => 'Angajati | Index',
    'menu_doc' => 'Documente angajati',
    'menu_add' => 'Adauga angajat'
];