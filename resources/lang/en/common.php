<?php
return [
    'welcome_c' => 'Welcome',
    'module' => 'Modules',
    'employees' => 'Employees',
    'ac_settings' => 'Account settings',
    'ac_actions' => 'Account actions',
    'logout' => 'Logout',
    'admin_error' => 'Error. Please contact your technical administrator.',
    'remove_request' => 'Are you sure you want to delete this record?',
    'close' => 'Close',
    'confirm'=> 'Confirm',
    'language' => 'Language',
    'a_p_settings' => 'Account/Profile Settings',
    'preferences' => 'Preferences',
    'done' => 'Done',
    'submit' => 'Submit',
    'update' => 'Update'

];