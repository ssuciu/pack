<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//Login routes


Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['web', 'auth']], function () {

    //Default route - Dashboard
    Route::get('/', "HomeController@redirectHome")->name('redirect');
    Route::get('/home', 'HomeController@index')->name('home');

    //Localization route
    Route::get('/language/{locale}', "LanguageController@changeLanguage")->name('locale');

    //Account/Profile routes
    Route::get('/profile', 'ProfileSettingsController@index')->name('profile');

    //Employees routes
    Route::resource('employees', 'EmployeesController');
    Route::post('/employees/list', 'EmployeesController@dataTable')->name('employees.datatable');
    Route::post('/employees/{employee}/documents','EmployeesController@documents')
        ->name('employees.upload-documents');
    Route::get('/employees/{employee}/documents/delete','EmployeesController@removeDocumentsView')
        ->name('employees.remove-documents-view');
    Route::get('/employees/{employee}/documents','EmployeesController@getDocuments')
        ->name('employees.add-documents');
    Route::get('/employees/{employee}/photos','EmployeesController@getPhotos')
        ->name('employees.photos');
    Route::delete('/employees/documents/{id}','EmployeesController@deleteDocument')
        ->name('employees.delete-document');
});