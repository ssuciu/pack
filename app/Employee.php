<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Employee extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'employees';
    protected $fillable = ['first_name','last_name', 'birthday', 'phone', 'email', 'address', 'ssn'];

    public static function deletePhoto($id)
    {
        $photo = DB::table('employees_photos')->where('id','=', $id);
        $photoDetails = $photo->first();
        $employee = $photoDetails->employee_id;
        $file = $photoDetails->file_name;
        Storage::delete('/public/documents/' . $employee . "/" . $file);
        $photo->delete();
        return true;
    }


    public function addDocument($name) {
        $id = $this->id;
        DB::table('employees_photos')->insert([
            'file_name' => $name,
            'description' => $name,
            'employee_id' => $id
        ]);
    }

    public function getPhotos()
    {
        $id = $this->id;
        $photos = DB::table('employees_photos')
            ->where('employee_id', '=', $id)
            ->get(['id', 'file_name'])
            ->toArray();
        return $photos;
    }

    public function hasDocuments() {
        $id = $this->id;
        $photos = DB::table('employees_photos')
            ->where('employee_id', '=', $id)
            ->get(['id'])
            ->count();
        return $photos;
    }
}
