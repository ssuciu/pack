<?php
/**
 * Created by PhpStorm.
 * User: suciu
 * Date: 16.11.2017
 * Time: 23:42
 */

namespace App;

use Illuminate\Support\Facades\DB;

class DataTable
{
    public static function getData($table, $input) {
        $perPage = 10;
        if(isset($input['datatable']['pagination']['perpage'])) {
            $perPage = $input['datatable']['pagination']['perpage'];
        }
        $page = $input['datatable']['pagination']['page'];
        $table = DB::table($table);
        $count = $table->count();
        $data = $table->skip($perPage * ($page - 1))->take($perPage)->get()->toArray();
        $meta = [
            'page' => (int) $page,
            'pages' => ceil($count/$perPage),
            'perPage' => $perPage,
            'total' => $count
        ];
        return ['data' => $data, 'meta'=> $meta];
    }
}