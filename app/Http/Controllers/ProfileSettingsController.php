<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileSettingsController extends Controller
{
    public function index() {
        return view('pages.profile-settings')
            ->with('user', Auth::user())->with('on_account_settings', true);
    }
}
