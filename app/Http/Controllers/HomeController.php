<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{


    /**
     * Redirects to Employees on "/".
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectHome(){
        return redirect("employees");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //We will redirect to employees for now
        //   return view('home')
        //       ->with('on_dashboard', true);
        return redirect("employees.index");

    }

}
