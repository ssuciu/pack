<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Helpers\DateConverter;
use App\Helpers\EmployeeFilter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.employees.index')->with('on_employees', true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.employees.create')->with('on_employees', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['birthday'] = DateConverter::dateToDBConverter($input['birthday']);
        $newEmployee = new Employee();
        $newEmployee->fill($input);
        $newEmployee->save();
        return view('pages.employees.add-photos-middle')
            ->with('employee', $newEmployee)->with('on_employees', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $employee->birthday = DateConverter::dateFromDBConverter($employee->birthday);
        return view('pages.employees.full-data')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $employee->birthday = DateConverter::dateFromDBConverter($employee->birthday);
        return view('pages.employees.update')
            ->with('employee', $employee)->with('on_employees', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $input = $request->all();
        $input['birthday'] = DateConverter::dateToDBConverter($input['birthday']);
        $employee->fill($input);
        $employee->save();
        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return json_encode(['status' => 1]);
    }


    public function dataTable(Request $request) {
        $search = $request->input();
        $datatable = new EmployeeFilter();
        $response = $datatable->filter($search);
        return json_encode($response);
    }

    public function documents(Request $request, Employee $employee) {
        $acceptedTypes = ['jpg', 'jpeg', 'png'];
        $mime = $request->file('file')->getClientMimeType();
        $size = $request->file('file')->getSize();
        if($size < 5000000) {
            $type = explode('/',$mime);
            if(count($type) === 2 && $type[0] === "image" && in_array($type[1], $acceptedTypes)) {
                $content = $request->file('file');
                if (!File::isDirectory(storage_path() . '/app/public/documents/' . $employee->id)) {
                    File::makeDirectory(storage_path(). '/app/public/documents/' . $employee->id);
                }
                $name = uniqid() . '.' . $type[1];
                $status = Storage::putFileAs('public/documents/' . $employee->id, $content, $name);
                if($status) {
                    $employee->addDocument($name);
                } else {
                    return response(json_encode('Error on file upload.'),400);
                }
            } else {
                return response(json_encode('Bad file type.'),400);
            }
        } else {
            return response(json_encode('File too big.'),400);
        }
    }

    public function getDocuments(Employee $employee) {
        return view('pages.employees.add-photos')->with('employee', $employee)->with('on_employees', true);
    }

    public function getPhotos(Employee $employee) {
        $photos = $employee->getPhotos();
        return response(json_encode($photos));
    }

    public function removeDocumentsView(Employee $employee) {
        $photos = $employee->getPhotos();
        return view('pages.employees.remove-photos')->with('photos',$photos)->with('employee', $employee)->with('on_employees', true);
    }

    public function deleteDocument($id) {
        $status = Employee::deletePhoto($id);
        if($status) {
            return response(json_encode(['status'=> 1]),200);
        } else {
            response(json_encode(['status' => 0]), 400);
        }
    }

}
