<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LanguageController extends Controller
{


    public function changeLanguage(Request $request, $language) {
        if(array_key_exists($language, config('languages'))) {
            $user = Auth::user();
            $user->preferred_language = $language;
            $user->save(['timestamps' => false]);
            session(['locale' => $language]);
            return redirect()->back();
        } else {
            return response(json_encode(['status' => 0, 'errors' => ['Language not available.']]), 404);
        }
    }
}