<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check() && !session()->has('locale')) {
            $locale = Auth::user()->preferred_language;
            session(['locale' => $locale]);
            app()->setLocale($locale);
        } else {
            app()->setLocale(session('locale', config('app.fallback_locale')));
        }
        return $next($request);
    }
}
