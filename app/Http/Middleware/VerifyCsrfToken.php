<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    protected function getTokenFromRequest($request)
    {

        $token = $request->input('datatable')['_token'];
        if($token) {
            if (! $token && $header = $request->header('X-XSRF-TOKEN')) {
                $token = $this->encrypter->decrypt($header);
            }
            return $token;
        }

        return parent::getTokenFromRequest($request); // TODO: Change the autogenerated stub
    }

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}
