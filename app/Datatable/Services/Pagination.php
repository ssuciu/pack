<?php

namespace App\Datatable\Services;

use Illuminate\Database\Query\Builder;

class Pagination
{
    /**
     * Add pagination to query builder
     *
     * @param Builder $qb
     * @param array $pagination Cheile sunt cele trimise de catre datatables
     * @return Builder
     */
    public static function setPagination(Builder $qb, array $pagination)
    {
        $assetsPerPage = (int)$pagination['perpage'];
        $offset = ((int)$pagination['page'] - 1) * $assetsPerPage;

        $qb->offset($offset)->limit($assetsPerPage);

        return $qb;
    }

    /**
     * Compute pagination response data
     *
     * @param $queryBuilder
     * @param $datatableFilter
     * @param $paginationOptions
     * @return array
     */
    public static function getPaginationResponseData($queryBuilder, $datatableFilter, $paginationOptions)
    {

        $noOfRecords = $paginationOptions['no_of_records'];
        $noOfPages = (int)ceil($noOfRecords / ($datatableFilter['datatable']['pagination']['perpage']));
        $page = (int)$datatableFilter['datatable']['pagination']['page'];

        if ($page > $noOfPages) {
            $page = $noOfPages;
        }

        $pagination =  array(
            "page"      => $page,
            "pages"     => $noOfPages,
            "perpage"   => (int)$datatableFilter['datatable']['pagination']['perpage'],
            "total"     => $noOfRecords,
            "sort"      => $paginationOptions['sort_direction'],
            "field"     => $paginationOptions['sort']
        );

        Pagination::setPagination($queryBuilder, $pagination);

        return $pagination;
    }


}