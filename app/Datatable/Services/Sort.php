<?php

namespace App\Datatable\Services;

use Illuminate\Database\Query\Builder;

class Sort
{
    /**
     * Adauga sortarea in qb
     *
     * @param Builder $qb
     * @param array $sort Cheile sunt cele trimise de catre datatables
     * @return Builder
     */
    public static function setOrderBy(Builder $qb, array $sort)
    {
        if ($sort['field'] == 'actions') {
            return $qb;
        }

        if (strlen($sort['field']) > 0 && strlen($sort['sort']) > 0) {
            $qb->orderBy($sort['field'], $sort['sort']);
        }

        return $qb;
    }
}