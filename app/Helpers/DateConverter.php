<?php
/**
 * Created by PhpStorm.
 * User: suciu
 * Date: 17.11.2017
 * Time: 22:39
 */

namespace App\Helpers;


class DateConverter
{
    public static function dateToDBConverter($originalDate) {
        $convertedDate = date("Y-m-d", strtotime($originalDate));
        return $convertedDate;
    }

    public static function dateFromDBConverter($originalDate) {
        $convertedDate = date("d-m-Y", strtotime($originalDate));
        return $convertedDate;
    }
}