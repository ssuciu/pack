<?php
namespace App\Helpers;

use App\Datatable\Services\Pagination;
use App\Datatable\Services\Sort;
use Illuminate\Support\Facades\DB;


class EmployeeFilter
{
    const RESTRICTED_FIELDS = [
        'actions'
    ];
    const FIELD_MAP = [
        'name' => 'first_name',
        'number' => 'id'
    ];

    public function getAlternative($field) {
        return isset(self::FIELD_MAP[$field]) ? self::FIELD_MAP[$field] : false;
    }

    public function filter($params) {
        $queryBuilder = DB::table('employees')
            ->whereNull('deleted_at');
        
        if (!isset($params['datatable']['sort'])) {
           $queryBuilder = $queryBuilder->orderBy('created_at', 'desc');
        };
        $queryBuilder = $queryBuilder
            ->select('id as number', 'first_name','last_name', 'birthday', 'phone', 'email', 'address', 'ssn', 'created_at');

        if (isset($params['datatable']['query'])) {
            $queryBuilder = $this->applyFilters($queryBuilder, $params['datatable']['query']);
        }

        $paginationOptions = array(
            'no_of_records' => $this->countFilterRoles($params),
            'sort' => 'name',
            'sort_direction' => 'asc'
        );

        $pagination = Pagination::getPaginationResponseData($queryBuilder, $params, $paginationOptions);

        if(!in_array($params['datatable']['sort'], self::RESTRICTED_FIELDS)) {
            $params['datatable']['sort']['field'] = $this->getAlternative($params['datatable']['sort']['field']) ?: $params['datatable']['sort']['field'];
            Sort::setOrderBy($queryBuilder, $params['datatable']['sort']);
        }

        $records = $queryBuilder->get()->toArray();
        foreach ($records as &$record) {
            $record->birthday = DateConverter::dateFromDBConverter($record->birthday);
        }

        return array(
            'meta' => $pagination,
            'data' => $records
        );
    }

    private function applyFilters($queryBuilder, $formData)
    {
        if (strlen($formData['name']) > 0) {
            $name = $formData['name'];
            $queryBuilder->where(function ($query) use ($name) {
                $query->where('first_name', 'like', '%' . $name . '%');
                $query->orWhere('last_name', 'like',  '%' . $name . '%');
            });
        }
        return $queryBuilder;
    }



    public function countFilterRoles(array $datatableFilter)
    {
        $queryBuilder = DB::table('employees')->whereNull('deleted_at');
        if (isset($datatableFilter['datatable']['query'])) {
            $this->applyFilters($queryBuilder, $datatableFilter['datatable']['query']);
        }

        return $queryBuilder->count();
    }


}